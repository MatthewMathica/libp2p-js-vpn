<div align="center">
<center>
  <a href="libp2p.io">
    <img width="250" src="https://github.com/libp2p/libp2p/blob/master/logo/black-bg-2.png?raw=true" alt="libp2p hex logo" />
  </a>
<center>
</div>
1
# libp2p-vpn-js
Implementation of simple vpn over libp2p running as systemd service

**TODO**:
- Persist address book
- DHT routing

### Install
``` bash
git clone --recurse-submodules https://gitlab.com/MatthewMathica/libp2p-js-vpn.git
cd libp2p-js-vpn
chmod +x build.sh
./build.sh
./up.sh -dev tun0 -ip 10.0.0.1/24 -key mykey.json 
```

