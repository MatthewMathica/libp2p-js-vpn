import express from 'express';
import bodyParser from 'body-parser';

import { JsonDB } from 'node-json-db';
import { Config } from 'node-json-db/dist/lib/JsonDBConfig.js';

const db = new JsonDB(new Config("db", true, false, '/'));

const app = express();
const port = 1337;

function initApi(node) {

    app.use(bodyParser.json());
    app.use('/static', express.static('gui'));

    app.post('/peers/save', (req, res) => {
        const { name, peerid } = req.body;
        db.push('/peers', {
            [name]: peerid
        }, false);
        res.json({
            text: 'ok'
        });
    });

    app.delete('/peers/:name', (req, res) => {
        db.delete(`/peers/${req.params.name}`);
        res.json({
            text: 'ok'
        });
    });

    app.get('/whoami', (req, res) => {
        res.json({
            peerid: node.peerId.toB58String()
        });
    });

    app.get('/peers', (req, res) => {
        res.json(db.getData('/peers'));
    });

    app.listen(port, () => {
        console.log(`api running on :${port}`);
    });
}

export { initApi };
