let tbody = null;
const table = document.getElementById('peers');

function makeCol(content) {
    const col = document.createElement('td');
    col.innerHTML = content;
    return col;
}

function makeRow(name, peerid) {
    const row = document.createElement('tr');
    row.setAttribute('id', name);
    row.append(
        makeCol(name),
        makeCol(peerid),
        makeCol('offline'),
        makeCol(
            `<button class="nes-btn is-primary" onclick="">info</button>
             <button class="nes-btn is-error" onclick="removePeer('${name}')">kick</button>`
        )
    );
    return row;
}

function addPeerToTable(name, peerid) {
    tbody = tbody ? tbody : document.createElement('tbody');
    tbody.append(makeRow(name, peerid));
    table.append(tbody);
}

function savePeer() {
    const name = document.getElementById('pname').value;
    const peerid = document.getElementById('pid').value;

    if (!name || !peerid) return;

    post('/peers/save', {
        name: name,
        peerid: peerid
    }).then(data => {
        addPeerToTable(name, peerid);
    });
}

function removePeer(name) {
    remove('/peers', name).then(_ => document.getElementById(name).remove());
}

get('/peers').then(data => {
    for (let key in data) {
        addPeerToTable(key, data[key]);
    }
});

get('/whoami').then(({peerid}) => {
    document.getElementById('my_peerid').innerHTML = peerid;
});
