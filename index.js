import PeerId from 'peer-id';
import { readJSON, writeJSON } from './utils.js';

import Node from './node.js';
import { initApi } from './api/index.js';

(async (_interface, _key, ..._) => {

    // PeerId
    let peerId = undefined;

    // Encryption scheme
    const opts = {
        bits: 2048,
        keyType: 'Ed25519'
    };

    // Path to keys
    const _keyPath = _key || process.env.KEY_PATH || './key.json';

    // Read keys from path
    const storedKeys = readJSON(_keyPath);

    if (storedKeys) {
        peerId = await PeerId.createFromJSON(storedKeys);
    }
    else {
        peerId = await PeerId.create(opts);
        writeJSON(_keyPath, peerId.toJSON());
    }

    // Start node
    const node = new Node(peerId);
    await node.start(_interface);

    // Start api
    initApi(node);

    console.log(`I'am ${node.peerId.toB58String()}`);

})(...process.argv.slice(2));
