// ------------------------------------------------
// LIBP2P
// ------------------------------------------------
import libp2p from 'libp2p';
import TCP from 'libp2p-tcp';
import MDNS from 'libp2p-mdns';
import MPLEX from 'libp2p-mplex';
import BOOTSTRAP from 'libp2p-bootstrap';
import Gossipsub from 'libp2p-gossipsub';
import KDHT from 'libp2p-kad-dht';
import PubsubPeerDiscovery from 'libp2p-pubsub-peer-discovery';
import { NOISE } from '@chainsafe/libp2p-noise';
// ------------------------------------------------
import { Protocol } from './protocol.js';
// ------------------------------------------------
// TUN interface
// ------------------------------------------------
import tunfd from 'tunfd';
// ------------------------------------------------
import fs from 'fs';
// ------------------------------------------------
import toIterable from 'stream-to-it';
// ------------------------------------------------
/*
  Name: Node v1.0.0
  Desc:
  - Exchanging duplex socket over libp2p-transports
*/
// ------------------------------------------------
export default class Node extends libp2p {
    constructor(peerId) {
        super({
            peerId,
            addresses: {
                listen: ['/ip4/0.0.0.0/tcp/7777']
            },
            modules: {
                transport: [TCP],
                streamMuxer: [MPLEX],
                pubsub: Gossipsub,
                dht: KDHT,
                connEncryption: [NOISE],
                peerDiscovery: [BOOTSTRAP, PubsubPeerDiscovery, MDNS]
            },
            config: {
                dht: {
                    enabled: true
                },
                peerDiscovery: {
                    autoDial: true,
                    bootstrap: {
                        interval: 1000,
                        enabled: true,
                        list: ['/ip4/93.114.133.44/tcp/7777/p2p/12D3KooWPo5Zu7WeeS8V3fmEhp57Ks71M5LNp6kzQutAXyrWf3B9']
                    }
                },
                relay: {
                    enabled: true,
                    hop: {
                        enabled: true,
                        active: true
                    }
                }
            }
        });

        this._hosts = undefined;
        this._protocol = undefined;
        this._interface = undefined;
    }

    // Start node
    async start(_interface) {
        // Start libp2p
        await super.start();
        try {
            // Start TUN interface
            this._interface = new tunfd.TunInterface({
                name: _interface,
                mode: 'tun',
                pi: false
            });

            // Read fd
            const fr = fs.createReadStream(null, { fd: this._interface.fd });
            // Write fd
            const fw = fs.createWriteStream(null, { fd: this._interface.fd });

            // Make it duplex
            const socket = {
                source: toIterable.source(fr),
                sink: toIterable.sink(fw)
            };

            // Register tunneling protocol over libp2p & socket
            this._protocol = new Protocol();
            this.handle(this._protocol.name, this._protocol.handle(socket));

            this.connectionManager.on('peer:connect', (connection) => {
                console.log(`Connected to: ${connection.remotePeer.toB58String()}`);
            });

            this.connectionManager.on('peer:disconnect', (connection) => {
                console.log(`Disconnected from: ${connection.remotePeer.toB58String()}`);
                this.peerStore.addressBook.delete(connection.remotePeer);
            });

            this.on('peer:discovery', (peerId) => {
                console.log(`Hey: ${peerId}`);
            });

            // TODO: Check if peerId is our friend
            // now we accept any node with the same protocol
            this.peerStore.on('change:protocols', async ({ peerId, protocols}) => {
                // stranger supports tunneling protocol
                if (protocols.includes(this._protocol.name)) {
                    // we initiate stream
                    const { stream } = await this.dialProtocol(peerId, [this._protocol.name]);
                    // exchange data
                    this._protocol.start(socket, stream);
                } else {
                    // if not decline connection
                    this.hangUp(peerId);
                    // Remove ${peerId} from address book
                    this.peerStore.addressBook.delete(peerId);
                }
            });

        } catch (err) {
            console.log('Couldn\'t start tun interface...');
            return err;
        }
        return 1;
    }
}
