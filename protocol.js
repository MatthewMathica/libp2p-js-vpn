import { pipe } from 'it-pipe';

const slice = chunks => (async function *(){
    for await (const chunk of chunks) {
        yield chunk.slice();
    }
})();

export class Protocol {
    name = '/protocol/1.0.0';

    constructor() {}

    async start(socket, stream) {
        console.log('Writing stream');
        pipe(
            socket,
            stream,
        );
    }

    handle(socket) {
        return async ({connection, stream}) => {
            console.log('Handling stream');
            pipe(
                stream,
                slice,
                socket
            );
        };
    };
}
