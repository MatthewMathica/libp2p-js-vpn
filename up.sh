#!/bin/bash

function usage(){
    echo "Usage:"
    echo "up.sh -dev <name> -ip <ip/mask> -key <path_to_key>"
    exit;
}

if [ $# -ne 6 ]; then
    usage;
fi

if [ "$1" != "-dev" ]; then
    usage;
fi

if [ "$3" != "-ip" ]; then
    usage;
fi

if [ "$5" != "-key" ]; then
    usage;
fi

DEV=$2
IP=$4
KEY=$6

sudo openvpn --mktun --dev $DEV
sudo ip link set $DEV up
sudo ip addr add $IP dev $DEV

node ./index.js $DEV $KEY
