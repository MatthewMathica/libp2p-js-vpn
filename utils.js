import fs from 'fs';
import PeerId from 'peer-id';

export const readJSON = (path) => {
    try {
        return JSON.parse(fs.readFileSync(path, {encoding:'utf8', flag:'r'}));
    } catch (err) {
        return null;
    }
};

export const writeJSON = (path, json) => {
    fs.writeFileSync(path, JSON.stringify(json));
};
